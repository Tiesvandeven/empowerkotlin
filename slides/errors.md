# Error handling

---slide---

## Things will fail

* Dropped connections
* Overflow

---slide---

## Error types

* Recoverable errors
  * Using a default value
  * Use an alternative code path
  * Return a different status code
* Unrecoverable (Technical errors)
  * Usually returns status code 500 

---slide---

## Default approaches

* Hope
* Runtime Exceptions

---slide---

## Runtime Exceptions

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">/**

* Will throw an ArithmeticException if b equals 0

*/

fun divide(a: Double, b: Double): Double {

  if (b == 0.0) {

    throw ArithmeticException()

  }

  return a / b

}

</code></pre>

---slide---

## Other options

* Nullability system
* Sealed types

---slide---

### Nullability system

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun divide(a: Double, b: Double): Double? {

  return if (b == 0.0) {

    null

  } else {

    return a / b
        
  }

}
</code></pre>

---slide---

### Recovering

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">//Compose

divide(1.0,0.0)?.let {

  // guaranteed not null

}
   

// Default 
    
val value = divide(1.0,0.0) ?: 0 


// Crash

val value2 = divide(1.0,0.0) ?: throw RuntimeException() 
</code></pre>


---slide---

#### Nullability system

* Showing the method can fail
  * Function has multiple return types
* Offering a higher context to recover
* Errors must be reasoned about
* Great for recoverable errors
* No error information

---slide---

### Modelling error state as a type

* Just like SignedContract
* Function return type is a
  * Success
  * Failure

---slide---

### Sealed types

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">sealed interface Result {
    
  data class Success(val value: Int) : Result

  data class Failure(val error: String) : Result

}
</code></pre>

---slide---

#### Example

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun divide(a: Int, b: Int): Result {

  return if (b == 0) {

    Result.Failure("b should not be 0")

  } else {

    Result.Success(a / b)

  }

}
</code></pre>
	
---slide---

#### Pattern matching

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun handle(input : Result) {


  var output = when (input) {


    is Result.Success -> "The value is:" + input.value;


    is Result.Failure -> "The error is:" + input.error;


  }


  println(output);

}
</code></pre>

---slide---

#### Alternatives

* Result
* Either/Effect (Arrow library)

---slide---

## Key take-aways

* Think about error paths
* Model them if they are recoverable
* Runtime exceptions are not the only option
