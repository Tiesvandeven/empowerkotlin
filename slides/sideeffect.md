# Side effect management

---slide---

## Pure functions

* Idempotent 
* Total
* Referentially transparent

---slide---

### Idempotent

* Repeatable
* Calling it with the same arguments gives the same result

---slide---

### Total

* Should have a valid output for every input

---slide---

### Referentially transparent

* Can be replaced by a lookup table
* fun plus(arg1 : Int, arg2 : Int) : Int
* Cannot alter state outside of the function
* Cannot use state from outside the function

<img style="width:500px;float:right;margin-right:20%" src="slides/images/plustable.svg">


---slide---

### Result

* Very reliable
* Consistent
* Easy to test
  * No mocks

---slide---

## Side effects

* I/O
  * Reading/writing files
  * Calling a webservice
  * Database operations
* Getters/setters
* Logging

---slide---


### Side effect problems

* Unreliable
* Hard to test

---slide---

### Hidden input/output

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun secondsBetweenNowAnd(date: LocalDateTime): Long {

  val now = LocalDateTime.now()
    
  return ChronoUnit.SECONDS.between(now, date)

}
</code></pre>

---slide---

### Side-effects as explicit input

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun secondsBetween(from: LocalDateTime, to: LocalDateTime): Long {

  return ChronoUnit.SECONDS.between(from, to)

}
</code></pre>

---slide---

### Logic with side-effects

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun foo(id: Int) {


  val user: User = findUserInDatabase(id)


  //logic

  if (user.name.startsWith("A")) {

    user.setAllowed(true)

  } else {

    user.setAllowed(false)

  }


  storeUserInDatabase(user)
}
</code></pre>
---slide---

### Separate Side-effects

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:150%;padding: 0.5em">fun foo(id: Int) {

  val user: User = findUserInDatabase(id)

  val modifiedUser = doBusinessLogic(user)

  storeUserInDatabase(modifiedUser)

}


fun doBusinessLogic(user: User): User {

  return if (user.name.startsWith("A")) {

    User(user.name, true)

  } else  {

    User(user.name, false)

  }

}
</code></pre>


---slide---

### Orchestrator pattern

<img style="width:1500px;" src="slides/images/orchestrator.svg">

---slide---

### Pure domain layer

* Pure domain layer
  * Reliable
  * Testable
* Anti-corruption layer
  * Handles side-effects

---slide---

<img style="width:1500px;" src="slides/images/hexagonal.svg">

---slide---

## Key take-aways

* You need side-effects
* Try to identify them
* Make them explicit
* Move them to the edges

