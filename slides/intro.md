# Empowering Your Development with Functional Programming

## Understanding and Practice

---slide---

# > WHOAMI

<img style="float: right; height: 500px;" src="slides/images/ties.jpg">

* Ties van de Ven
* Java/Kotlin/Scala
* Functional Programming advocate

---slide---

# Agenda

* Intro
* Why FP?
* Composition
* State management
* Error management
* Side effect management

---slide---

# Physics vs Math

<img style="width:1000px;" src="slides/images/physicsmath.svg">

---slide---

# Why FP?

---slide---

## Reusability

---slide---

## Terminology

---slide---

## Complexity management

---slide---

<img src="slides/images/scale2.svg">


