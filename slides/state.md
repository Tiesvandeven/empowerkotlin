# State management

---slide---

## What is state?
Values within your application

---slide---

## Added complexity

<img style="float: left; height: 300px;  transform: scaleX(-1);" src="slides/images/redcar.png">
<img style="float: right; height: 300px;" src="slides/images/bluecar.png">


---slide---

## OO Car

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">class Car {

    var colour: String? = null
	
}
</code></pre>

---slide---

## FP Car

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">class Car(val colour: String) {


}
</code></pre>

---slide---

## Time

> value = (state, time)

---slide---

## Problems with state

* Complex
	* Race conditions
	* Concurrency
* High cognitive load

---slide---

## Cognitive load

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun foo(redCar : Car) {


  println(redCar);
  
  
  paint(redCar);
  
  
  println(redCar);
  
  
}
</code></pre>

---slide---

## Immutable objects

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">data class ImmutableCar(val colour : String, val brand : String)
</code></pre>

---slide---

### Cognitive load

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun foo(redCar : ImmutableCar) {


  println(redCar);
  
  
  paint(redCar);
  
  
  println(redCar);
  
  
}
</code></pre>

---slide---

### Changing values

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun paintBlue(car : ImmutableCar) = ImmutableCar("Blue", car.brand)


fun paintBlue(car : ImmutableCar) = car.copy(colour = "Blue")

</code></pre>
		
---slide---

### Explicit state change

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun foo(redCar : ImmutableCar) {


  println(redCar);
  
  
  val blueCar = paintBlue(redCar);
  
  
  println(blueCar);
  
  
}
</code></pre>

---slide---
	
## Tradeoffs

* Consistency vs Performance
	
---slide---
	
### Performance
	
---slide---
	
### Dealing with tradeoffs	

* Function signatures should be immutable
* Keep mutable state function scoped

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun foo(input: String): String {

  val builder = StringBuilder(input)

  for (i in 0..1_000_000) {

    builder.append("0")

  }

  return builder.toString()

}
</code></pre>

---slide---

## Making illegal states unrepresentable

* Making sure the content of the state is correct


---slide---

## Nullable fields

---slide---

### Optional fields

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">data class Person(


  val firstName : String,
  
  
  val middleName : String?,
  
  
  val lastName : String
  
)
</code></pre>

---slide---

### Optional fields?

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">data class Contract(


  val text: String, 


  val signature: String?


)
</code></pre>

---slide---

### Effect

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun startProject(contract: Contract) {

  checkNotNull(contract.signature)

  ...

}
</code></pre>

---slide---

### State changes as types

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">data class Contract(

  val text: String

)


data class SignedContract(

  val text: String, 

  val signature: String

)
</code></pre>

---slide---

### Types as architecture

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">fun startProject(contract: SignedContract) {

  // No null check needed

  ...

}
</code></pre>

---slide---

## Key take-aways

* State is difficult
* Mutability adds the complexity of time
* Mutable state is not inherently bad
* We can use types to remove null values
