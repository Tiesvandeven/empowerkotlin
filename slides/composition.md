# Composition

---slide---

## Function Signatures

<pre>
<code class="hljs kotlin" style="max-height: 200%;font-size:200%;padding: 0.5em">fun getName(input : Person) : String { ... } // Person -> String


fun length(input : String) { ... } // String -> Int


length(getName(input)) // Person -> Int
</code>
</pre>

---slide---

### Domino

<img style="width:1000px;" src="slides/images/domino.jpg">

---slide---

### In code

> Given a list of Person's sum the total age of everyone with a name starting with "A"

---slide---

### Using for

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">var totalLength = 0
    
for (p in persons) {

	if (p.getName().startsWith("A")) {
	
		totalLength += p.getAge()
		
	}
	
}
</code></pre>

---slide---

### Breaking down the problem

> Given a list of Person's sum the total age of everyone with a name starting with "A"

* Only select people with a name starting with "A"
* Get the age of the person
* Sum all ages

---slide---

### Using streams

<pre><code class="hljs kotlin" style="max-height: 100%;font-size:200%;padding: 0.5em">val totalLength = persons

        .filter { it.name.startsWith("A") }
		
        .map { it.age }
		
        .sum()
</code></pre>

---slide---

### Bash

> ls | grep a

---slide---

## Composable Problem solving

* Programming is problem solving
* Problems can be divided

---slide---

<img style="width:1000px;" src="slides/images/design.svg">
		
---slide---

<img style="width:1500px;" src="slides/images/design2.svg">
		
---slide---

<img style="width:1500px;" src="slides/images/design3.svg">

---slide---

## Key take-aways

* Problems are compositions of sub-problems
* Consider streaming API
